<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password'  => 'A password deve conter pelo menos oito caracteres e ser igual à confirmação.',
    'reset'     => 'Sua password foi redefinida!',
    'sent'      => 'Enviamos um link para redefinir a sua password por e-mail.',
    'throttled' => 'Por favor espere antes de tentar novamente.',
    'token'     => 'Esse código de redefinição de password é inválido.',
    'user'      => 'Não conseguimos encontrar nenhum usuário com o endereço de e-mail informado.',
];
