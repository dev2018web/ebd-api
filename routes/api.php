<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('auth/login', 'AuthController@login');
Route::post('auth/logout', 'AuthController@logout');
Route::post('auth/refresh', 'AuthController@refresh');
Route::post('auth/me', 'AuthController@me');
Route::post('auth/register', 'AuthController@register');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/teste', 'AtividadeController@notifyStudentAboutActivity');

Route::middleware(['apiJWT', 'admin'])->prefix('/admin')->group(function () {
    Route::apiResource('/usuario', 'UserController');
    Route::apiResource('/escola', 'EscolaController');
    Route::apiResource('/classe', 'ClasseController');
    Route::apiResource('/licao', 'LicaoController');
    Route::apiResource('/aula', 'AulaController');
    Route::apiResource('/conteudo', 'ConteudoController');
    Route::apiResource('/atividade', 'AtividadeController');
    Route::apiResource('/questao', 'QuestaoController');
    Route::apiResource('/submissao', 'SubmissaoController');
    Route::apiResource('/alternativa', 'AlternativaController');
    Route::apiResource('/categoria', 'CategoriaController');
    
    Route::prefix('/escola')->group(function () {
        Route::prefix('/aluno')->group(function () {
            Route::post('', 'EscolaController@setStudentInSchool');
            Route::delete('/remove', 'EscolaController@removeStudentFromSchool');
        });

        Route::prefix('/atividade')->group(function () {
            Route::post('', 'EscolaController@setAtividadeInEscola');
            Route::delete('/remove', 'EscolaController@removeAtividadeFromEscolaAula');
        });

        Route::prefix('/aula')->group(function () {
            Route::prefix('/atividade')->group(function () {
                Route::get('', 'EscolaController@ActivitiesInAulaSchool');
            });
        });
    });

    Route::prefix('/classe')->group(function () {
        Route::prefix('/aluno')->group(function () {
            Route::post('', 'ClasseController@setUserInClasse');
            Route::delete('remove', 'ClasseController@removeStudentFromClass');
        });

        Route::get('/{classe_id}/nota', 'ClasseController@listStudentsWithPontuations');

        Route::prefix('/professor')->group(function () {
            Route::post('', 'ClasseController@setUserInClasse');
            Route::delete('remove', 'ClasseController@removeProfessorFromClasse');
        });

        Route::prefix('/aula')->group(function () {
            Route::prefix('/atividade')->group(function () {
                Route::get('', 'ClasseController@ActivitiesInAulaClass');
            });
        });

        // coloquei o list porqeu não estava funcionando sem ele.
        Route::get('/atividades/list/{class_id}', 'AtividadeController@listActivitiesOfStudentInClass');

        Route::post('/start', 'ClasseController@startAula');
    });

    Route::post('questao/answer', 'QuestaoController@answerQuestion');

    Route::prefix('/aula')->group(function () {
        Route::prefix('/atividade')->group(function () {
            Route::post('', 'AulaController@setAtividadeInAula');
            Route::delete('remove', 'AulaController@removeAtividadeFromAula');
        });
    });

    Route::prefix('/licao')->group(function () {
        Route::prefix('/escola')->group(function () {
            Route::post('', 'LicaoController@setLessonInSchool');
        });
        Route::post('/{licao_id}', 'LicaoController@setLessonInClass');
    });

    Route::prefix('/atividade')->group(function () {
        Route::post('/deliver', 'AtividadeController@deliverActivity');
        Route::post('/note', 'AtividadeController@putNoteInSubmissionActitivity');
    });
});

Route::middleware(['apiJWT', 'gestor'])->prefix('/gestor')->group(function () {
    Route::apiResource('/usuario', 'UserController');
    Route::apiResource('/classe', 'ClasseController');
    Route::apiResource('/licao', 'LicaoController');
    Route::apiResource('/conteudo', 'ConteudoController');
    Route::apiResource('/atividade', 'AtividadeController');
    Route::apiResource('/submissao', 'SubmissaoController');
    Route::apiResource('/questao', 'QuestaoController');
    Route::apiResource('/alternativa', 'AlternativaController');

    Route::apiResource('/escola', 'EscolaController')->except([
        'store', 'destroy', 'index'
    ]);
    
    Route::prefix('/escola')->group(function () {
        Route::prefix('/aluno')->group(function () {
            Route::post('', 'EscolaController@setStudentInSchool');
            Route::delete('/remove', 'EscolaController@removeStudentFromSchool');
        });

        Route::prefix('/atividade')->group(function () {
            Route::post('', 'EscolaController@setAtividadeInEscola');
            Route::delete('/remove', 'EscolaController@removeAtividadeFromEscolaAula');
        });

        Route::prefix('/aula')->group(function () {
            Route::prefix('/atividade')->group(function () {
                Route::get('', 'EscolaController@ActivitiesInAulaSchool');
            });
        });

        Route::prefix('/aula')->group(function () {
            Route::prefix('/atividade')->group(function () {
                Route::get('', 'ClasseController@ActivitiesInAulaClass');
            });
        });
    });

    Route::prefix('/licao')->group(function () {
        Route::prefix('/escola')->group(function () {
            Route::post('', 'LicaoController@setLessonInSchool');
        });
        Route::post('/{licao_id}', 'LicaoController@setLessonInClass');
    });

    Route::prefix('/classe')->group(function () {
        Route::prefix('/aluno')->group(function () {
            Route::post('', 'ClasseController@setUserInClasse');
            Route::delete('remove', 'ClasseController@removeStudentFromClass');
        });

        Route::prefix('/professor')->group(function () {
            Route::post('', 'ClasseController@setUserInClasse');
            Route::delete('remove', 'ClasseController@removeProfessorFromClasse');
        });

        Route::post('/start', 'ClasseController@startAula');

        Route::prefix('/aula')->group(function () {
            Route::prefix('/atividade')->group(function () {
                Route::get('', 'ClasseController@ActivitiesInAulaClass');
            });
        });
    });

    Route::prefix('/atividade')->group(function () {
        Route::post('/deliver', 'AtividadeController@deliverActivity');
        Route::post('/note', 'AtividadeController@putNoteInSubmissionActitivity');
    });
    
    Route::post('questao/answer', 'QuestaoController@answerQuestion');
});

Route::middleware(['apiJWT', 'professor'])->prefix('/professor')->group(function () {
    Route::apiResource('/conteudo', 'ConteudoController');
    Route::apiResource('/atividade', 'AtividadeController');
    Route::apiResource('/submissao', 'SubmissaoController')->except([
        'index'
    ]);
    Route::apiResource('/questao', 'QuestaoController')->except([
        'index'
    ]);
    Route::apiResource('/alternativa', 'AlternativaController')->except([
        'index'
    ]);
    
    Route::apiResource('/usuario', 'UserController')->except([
        'store', 'destroy'
    ]);

    Route::apiResource('/classe', 'ClasseController')->except([
        'store', 'destroy', 'index'
    ]);

    Route::apiResource('/licao', 'LicaoController')->except([
        'store', 'destroy'
    ]);

    Route::prefix('/classe')->group(function () {
        Route::prefix('/aluno')->group(function () {
            Route::post('', 'ClasseController@setUserInClasse');
            Route::delete('remove', 'ClasseController@removeStudentFromClass');
        });

        Route::post('/start', 'ClasseController@startAula');

        Route::prefix('/aula')->group(function () {
            Route::prefix('/atividade')->group(function () {
                Route::get('', 'ClasseController@ActivitiesInAulaClass');
            });
        });
    });

    Route::prefix('/atividade')->group(function () {
        Route::post('/deliver', 'AtividadeController@deliverActivity');
    });

    Route::prefix('/submissao')->group(function () {
        Route::post('/note', 'SubmissaoController@putNoteInSubmissionActitivity');
    });

    Route::post('questao/answer', 'QuestaoController@answerQuestion');
});

Route::middleware(['apiJWT', 'aluno'])->prefix('/aluno')->group(function () {
    Route::post('dashboard', 'StudentController@getDashboard');
    
    Route::get('/atividade/{atividade_id}', 'AtividadeController@show');
    Route::put('/submissao', 'SubmissaoController@store');
    Route::get('/submissao', 'SubmissaoController@store');


    Route::apiResource('/classe', 'ClasseController')->except([
        'store', 'destroy', 'index'
    ]);

    Route::prefix('/classe')->group(function () {
       // coloquei o list porqeu não estava funcionando sem ele.
       Route::get('/atividades/list', 'AtividadeController@listActivitiesOfStudentInClass');
       Route::get('/atividade/corrected', 'AtividadeController@showCorrectedActivity');
    });
 
    Route::get('/classe/{classe_id}/nota', 'ClasseController@listStudentsWithPontuations');

    Route::apiResource('/licao', 'LicaoController')->except([
        'store', 'destroy'
    ]);

    Route::prefix('/atividade')->group(function () {
        Route::post('/deliver', 'AtividadeController@deliverActivity');
    });

    Route::post('questao/answer', 'QuestaoController@answerQuestion');
});
