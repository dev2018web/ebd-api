<?php

namespace App\Http\Middleware;
use App\Models\User;
use Closure;

class AlunoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(User::userLogged()->role==="ALUNO"){
            return $next($request);
        }else{
            return response()->json(['error'=>'Função não autorizada para este usuário'], 401);
        }
    }
}
