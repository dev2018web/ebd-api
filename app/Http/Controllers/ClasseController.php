<?php

namespace App\Http\Controllers;

use App\Models\Classe;
use App\Models\Escola;
use App\Models\Chamada;
use App\Models\RL\UsuarioClasse;
use App\Models\RL\UsuarioEscola;
use App\Models\RL\LessionadaClasse;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ClasseController extends Controller
{
    /**
     * Lista as classes paginadas
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $classes = Classe::where('titulo', 'like', "%" . $request->get("s") . "%")->with(['aulas.atividades', 'categoria', 'alunos' => function ($query) {
                $query->where("role", "ALUNO");
            }])->paginate(10);

            return response()->json($classes, 200);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Cria uma nova classe
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->post(), [
                'titulo' => 'required',
                'id_classe_categoria' => 'required|exists:App\Models\Categoria,id',
                'escola_id' => 'required|exists:App\Models\Escola,id',
                'nummax_professores' => 'required|integer',
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            } else {
                extract($request->post());

                $data = [
                    'titulo' => $titulo,
                    'id_classe_categoria' => $id_classe_categoria,
                    'escola_id' => $escola_id,
                    'nummax_professores' => $nummax_professores,
                ];

                if (isset($request->foto) && $request->file('foto')->isValid()) {
                    $path = $request->foto->store('images/classe');

                    $data['foto'] = $path;
                }

                $classe = Classe::create($data);

                return response()->json($classe, 200);
            }
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Exibe uma classe escolhida, com os seus respectivos alunos.
     *
     * @param  \App\Models\Classe  $classe
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Classe $classe)
    {
        try {
            $classe = $classe->with(['categoria', 'licao.aulas', 'alunos' => function ($query) {
                $query->where("usuario_classe.role", "ALUNO");
            }])->firstOrFail();

            return response()->json($classe, 200);
        } catch (\Exception $ex) {
            return $ex;
        }
    }

    /**
     * atualiza os dados cadastrais de uma escola. Nesse caso, os dados editáveis são:
     * Nome da classe;
     * Categoria.
     *
     * @param  \App\Models\Classe  $classe
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, int $classe_id)
    {
        try {
            $validator = Validator::make($request->post(), [
                'titulo' => 'required',
                'id_classe_categoria' => 'required',
                'escola_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            } else {
                extract($request->post());

                $data = [
                    'titulo' => $titulo,
                    'id_classe_categoria' => $id_classe_categoria,
                    "escola_id" => $escola_id,
                ];

                $escolas = Classe::find($classe_id)->update($data);
            }

            return response()->json(['updated' => true], 200);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Remove uma classe específica
     *
     * @param  int $classe_id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $classe_id)
    {
        try {
            $classe = Classe::find($classe_id);

            if ($classe) {
                $classe->delete();
            } else {
                return response()->json(["error" => "classe já foi removida"], 400);
            }

            return response()->json($classe, 200);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    public function removeStudentFromClass(Request $request)
    {
        try {
            $message = "";
            $status = 400;

            $validator = Validator::make($request->post(), [
                'id_aluno' => 'required|exists:App\Models\User,id',
                'id_classe' => 'required|exists:App\Models\Classe,id',
            ]);
            if ($validator->fails()) {
                $message = $validator->errors();
            } else {
                extract($request->post());

                if (ClasseRL::find($id_classe)) {
                    $data = [
                        'classe_id' => $id_classe,
                        'aluno_id' => $id_aluno,
                    ];

                    $escola_aluno = ClasseRL::where($data)->delete();

                    $message = ["msg" => "aluno removido da classe com sucesso!"];
                    $status = 200;
                } else {
                    $message = ["error" => "esse aluno não faz parte dessa classe"];
                }
            }

            return response()->json($message, $status);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Coloca um professor ou aluno em uma determinada classe
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function setUserInClasse(Request $request)
    {
        try {
            $message = "";
            $status = 200;
            $error = "";

            $validator = Validator::make($request->post(), [
                "classe_id" => 'integer|required|exists:App\Models\Classe,id',
                "usuario_id" => 'integer|required|exists:App\Models\User,id',
                "tipo_usuario" => 'string|required|in:PROFESSOR,ALUNO',
            ]);

            if ($validator->fails()) {
                $message = $validator->errors();
                $status = 400;
            } else {
                $userLoggedRole = User::userLogged()->role;

                /* Tipos que cada usuário pode cadastrar de acordo com seu perfil no sistema */
                $can_register = ['PROFESSOR', 'ALUNO'];
                switch ($userLoggedRole) {
                    case 'PROFESSOR':
                        unset($can_register[0]);
                        break;
                    case 'ALUNO':
                        $can_register = [];
                        break;
                }

                extract($request->post());
                $userRequestedRole = User::find($usuario_id)->role;

                /* Verifica se o usuário cadastrado no banco possui a mesma role passada pelo requester */
                $is_role_correct = ($userRequestedRole === $tipo_usuario);

                if (\in_array($userRequestedRole, $can_register) && $is_role_correct) {
                    $data_usuario_escola = [
                        'escola_id' => Classe::find($classe_id)->escola->id,
                        'usuario_id' => $usuario_id,
                    ];
                    /* Verificar se o usuário faz parte da escola */
                    $error = ["error" => "Usuário não está matriculado na escola"];
                    $message = (!UsuarioEscola::where($data_usuario_escola)->first() ? $error : '');

                    $usuarioClasseData = [
                        'usuario_id' => $usuario_id,
                        'classe_id' => $classe_id,
                        'role' => $tipo_usuario,
                    ];

                    /* Verifica se o usuário já faz parte da classe */
                    $error = ["error" => "Esse usuário já faz parte dessa classe"];
                    !$message ? $message = (UsuarioClasse::where($usuarioClasseData)->first() ? $error : '') : null;

                    if ($userRequestedRole === 'PROFESSOR' && !$message) {
                        /* Verifica se ainda tem vaga na classe para cadastrar algum professor */
                        if ($this->isPossibleRegisterUserInClass($classe_id, $userRequestedRole)) {
                            UsuarioClasse::create($usuarioClasseData);
                            $message = ['sucesso' => true];
                            $status = 202;
                        } else {
                            $message = ['msg' => 'Classe atingiu o número limite de professores'];
                        }
                    } elseif ($userRequestedRole == 'ALUNO' && !$message) {
                        UsuarioClasse::create($usuarioClasseData);
                        $message = ['sucesso' => true];
                        $status = 202;
                    } elseif (!$message) {
                        $message = ["error" => 'Esse tipo de usuário não pode ser cadastrado na classe.'];
                    }
                } else {
                    $message = ["error" => 'O tipo do usuário passado não está correto.'];
                    $status = 400;
                }
            }

            return response()->json($message, $status);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }


    public function removeProfessorFromClasse(Request $request)
    {
        try {
            $message = "";
            $status = 400;

            $validator = Validator::make($request->post(), [
                'id_aluno' => 'required|exists:App\Models\User,id',
                'id_classe' => 'required|exists:App\Models\Classe,id',
            ]);
            if ($validator->fails()) {
                $message = $validator->errors();
            } else {
                extract($request->post());

                if (ClasseRL::find($id_classe)) {
                    $data = [
                        'classe_id' => $id_classe,
                        'aluno_id' => $id_aluno,
                    ];

                    $escola_aluno = ClasseRL::where($data)->delete();

                    $message = ["msg" => "aluno removido da classe com sucesso!"];
                    $status = 200;
                } else {
                    $message = ["error" => "esse aluno não faz parte dessa classe"];
                }
            }

            return response()->json($message, $status);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Lista uma aula dentro de uma classe. Essa lição vem com as atividades
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function ActivitiesInAulaClass(Request $request)
    {
        try {
            $validator = Validator::make($request->post(), [
                'aula_id' => 'required|exists:App\Models\Aula,id',
                'classe_id' => 'required|exists:App\Models\Classe,id',
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            } else {
                $aulaInClass = Classe::getActivitiesInAulaClass(['aula_id'=>$request->post('aula_id'), 'classe_id'=>$request->post('classe_id')]);

                return response()->json($aulaInClass, 200);
            }
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Determina que uma aula está iniciada e prepara as chamadas
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function startAula(Request $request)
    {
        try {
            $message = "";
            $status = 400;

            $validator = Validator::make($request->post(), [
                'aula_id' => 'required|exists:App\Models\Aula,id',
                'classe_id' => 'required|exists:App\Models\Classe,id',
            ]);

            if ($validator->fails()) {
                $message = $validator->errors();
            } else {
                // Verifica se o usuário está tentando iniciar aula pela segunda vez
                if (!LessionadaClasse::where($request->post())->first()) {
                    // Determina que a aula foi iniciada
                    $message = LessionadaClasse::create($request->post());
                    $alunos = Classe::find($request->post('classe_id'))->alunos->where('role', "ALUNO");

                    $chamadas = [];

                    // Criar lista de chamadas assim que
                    $chamadas[] = collect($alunos)->map(function ($aluno) {
                        return Chamada::create(['aluno_id' => $aluno->id]);
                    });

                    $status = 200;
                } else {
                    $message = ['error'=>'Essa aula já foi iniciada!'];
                    $status = 400;
                }
                return response()->json($message, $status);
            }

            return response()->json($message, $status);
        } catch (\Exception $ex) {
            return $ex;
        }
    }

    public function listStudentsWithPontuations(int $class_id): object
    {
        try {
            $classe = Classe::with('alunos.pontuations', 'licoes.aulas.atividades')->where('id', $class_id)->first();

            $num_activities = 0;
            foreach($classe->licoes as $licao){
                foreach($licao->aulas as $aula){
                    foreach($aula->atividades as $atividade){
                        $num_activities++;
                    }
                }
            }

            $pontuations = $classe->alunos->map(function($aluno) use ($num_activities) {
                $data = [];
                $data['code'] = substr(md5(time() . uniqid()), 0,4);
                $data['user_name'] = $aluno->nome;
                $data['user_id'] = $aluno->id;
                $data['notas'] = $aluno->pontuations->map(fn($pontuation) => $pontuation->nota)->toArray();
                $data['media'] = array_sum($data['notas']) / $num_activities;

                return $data;
            });
            
            return response()->json($pontuations, 200);
        } catch (\Exception $ex) {
            return $ex;
        }
    }

    private function isPossibleRegisterUserInClass($classe_id, $userRequestedType)
    {
        /* Pega o número máximo de professores dentro de uma classe */
        $numMaxTeachersInClasse = Classe::find($classe_id)->nummax_professores;

        /*Verifica quantos alunos tem em uma determinada classe*/
        $numTeacherRegisteredInClasse = User::where('role', $userRequestedType)
            ->whereHas('classe', function (Builder $query) use ($classe_id) {
                return $query->where('classe_id', '=', $classe_id);
            })->count();

        return $numTeacherRegisteredInClasse < $numMaxTeachersInClasse;
    }
}
