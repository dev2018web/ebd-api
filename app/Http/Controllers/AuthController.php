<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\QueryException;

class AuthController extends Controller
{
     /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {   
        $response = "";
        $status = 400;

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
              $status = 400;

              return response()->json(['error'=>'Você precisa passar e-mail e senha.',$validator->errors()->toArray(), 'status'=>$status], $status);
        }else {
            $credentials = $request->only(['email', 'password']);

            if (!$response = auth('api')->attempt($credentials)) {
                $response = ['error' => 'Login e/ou password incorreto(s)!'];
                $response['status'] = 401;

                return response()->json($response, $response['status']);
            }else{
                $status = 200;
            }
        }

        return $this->respondWithToken($response, $status);
    }

     /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {   
        try{
            $response = "";
            $status = 400;  
            
            $validator = Validator::make($request->post(), [
                'nome' => 'required',
                'email' => 'required|unique:usuarios,email|email',
                'password' => 'required',
                'role' => 'required|in:ADMIN,GESTOR,PROFESSOR,ALUNO',
                'data_nascimento' => 'date_format:"d/m/Y"|required|date',
                'profile_img' => 'required'
            ]);     

            if ($validator->fails()) {
                return response()->json(['error'=>'Você precisa passar todos os campos.', $validator->errors()->toArray(), 'status'=>$status], $status);
            }else {
                $name = md5(rand(1, 999).uniqid().time());
                $path = "images/{$name}.jpg";
                Storage::put($path, base64_decode($request['profile_img']));
                    
                $data_usuario = $request->only([
                    'nome',
                    'email',
                    'role',
                    'data_nascimento',
                    'status' => 'ACTIVE',
                    'endereco_id' => 1
                ]);
                    
                $data_usuario['foto'] = $path;

                $data_usuario['password']  = Hash::make($request->post('password'));
                $data_usuario['data_nascimento'] = date("Y-m-d", strtotime($request->post('data_nascimento')));
                // return response()->json($data_usuario, 200);

                User::create($data_usuario);

                $response = auth('api')->attempt($request->only(['email', 'password']));
                return $this->respondWithToken($response, $status);
            }
        }catch(QueryException $ex){
            return response()->json($ex, 200);
        }catch(Exception $ex){
            return response()->json($ex, 200);
        }
    }
    

    protected function respondWithToken($token, $status = 200)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'user' => User::userLogged()
        ], $status);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth('api')->refresh());
    }

     /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth('api')->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('api')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

}
