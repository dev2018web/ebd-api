<?php

namespace App\Http\Controllers;

use App\Models\Alternativa;
use App\Models\Questao;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AlternativaController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->post(), [
                'texto' => 'required',
                'questao_id' => 'required|exists:App\Models\Questao,id',
                'correta' => 'required|in:0,1'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            } else {
                extract($request->post());

                if ($correta == 1) {
                    $questao = Questao::with('alternativas')->where(['id' => $questao_id])->first();

                    if (!empty($questao)) {
                        $multipla_escolha = $questao->multipla_escolha;
                
                        if (!$multipla_escolha) {
                            $dados = collect($questao->alternativas)->filter(function ($data) {
                                return $data->correta === 1;
                            })->reject(function ($data) {
                                return empty($data);
                            });

                            if (count($dados) > 0) {
                                return response()->json(["error"=>"Essa questão não pode ter mais de uma alternativa correta, pois não é de múltipla escolha"], 400);
                            }
                        }
                    } else {
                        return response()->json(["error"=>"Ops! Você está tentando adicionar alternativa em uma questão que foi apagada"], 400);
                    }
                }

                $questao = Alternativa::create($request->post());

                return response()->json($questao, 200);
            }
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Alternativa  $alternativa
     * @return \Illuminate\Http\Response
     */
    public function show(Alternativa $alternativa)
    {
        try {
            return response()->json($alternativa, 200);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Alternativa  $alternativa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Alternativa $alternativa)
    {
        try {
            $validator = Validator::make($request->post(), [
                'texto' => 'required',
                'correta' => 'required|in:0,1'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            } else {
                extract($request->post());

                $data = [
                    'texto' => $texto,
                    'correta' => $correta
                ];

                $alternativa->update($data);
            }

            return response()->json($alternativa, 200);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Alternativa  $alternativa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Alternativa $alternativa)
    {
        try {
            $alternativa->delete();

            return response()->json($alternativa, 200);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }
}
