<?php

namespace App\Http\Controllers;

use App\Models\Endereco;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EnderecoController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($dados)
    {   

        $validator = Validator::make($dados, [
            "cep" => "required",
            "logradouro" => "required",
            "complemento" => "required",
            "bairro" => "required",
            "localidade" => "required",
            "uf" => "required"
        ]);

        if($validator->fails()){

            echo ($validator->errors());
            return -1;
        }else{
            $endereco = Endereco::create($dados);
            return $endereco->id;
        }

    }
}
