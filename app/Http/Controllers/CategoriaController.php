<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoriaController extends Controller
{
    /**
     * Lista as categorias
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $classes = Categoria::all();

            return response()->json($classes, 200);
        }catch(\Exception $ex){
            return response($ex, 500);
        }
    }

    /**
     * Cria uma categoria para classe
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try{
            $validator = Validator::make($request->post(), [
                'nome' => 'required',
            ]);

            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            }else{
                extract($request->post());

                $data = [
                    'nome' => $nome,
                ];

                $categoria = Categoria::create($data);
            }

            return response()->json($categoria, 200);
        }catch(\Exception $ex){
            return response($ex, 500);
        }
    }

    /**
     * Lista uma categoria
     *
     * @param  int $categoria_id
     * @return \Illuminate\Http\Response
     */
    public function show(int $categoria_id)
    {
        try{
            $categorias = Categoria::find($categoria_id);

            return response()->json($categorias, 200);
        }catch(\Exception $ex){
            return response($ex,500);
        }
    }

    /**
     * Atualiza uma categoria
     *
     * @param  int $categoria_id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, int $categoria_id)
    {
        try{
            $validator = Validator::make($request->post(), [
                'nome' => 'required',
            ]);

            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            }else{
                extract($request->post());

                $data = [
                    'nome' => $nome,
                ];

                $categoria = Categoria::find($categoria_id)->update($data);
            }

            return response()->json(['updated'=>true], 200);
        }catch(\Exception $ex){
            return response($ex, 200);
        }
    }


    /**
     * Remove uma categoria específica
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        try{
            $categoria = Categoria::find($id);

            if($categoria){
                $categoria->delete();
            }else{
                return response()->json(["error"=> "Categoria já foi removida"], 400);
            }

            return response()->json($categoria, 200);
        }catch(\Exception $ex){
            return response($ex, 500);
        }
    }
}
