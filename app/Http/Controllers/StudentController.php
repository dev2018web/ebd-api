<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Escola;
use App\Models\Classe;
use App\Models\Questao;
use Illuminate\Support\Facades\Validator;

class StudentController extends Controller
{
    public function getDashboard(Request $request){
        try{
            $validator = Validator::make($request->post(), [
                "school_id" => "required",
                "class_id" => "required",
                "user_id" => "required"
            ]);
    
            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            }else{
                $dashboard = Escola::where('id', $request->post('school_id'))->with(['classe' => function ($query) use ($request) {
                    return $query->where('id', $request->post('class_id'));
                }])->first();

                $dashboard->classe->atividades = Classe::getActivitiesInClassWithNote(['classe_id'=>$request->post('class_id')], $request->post('user_id'))->map(function($atividade){
                    $atividade->questoes = Questao::where('atividade_id', $atividade->id)->with('alternativas')->get();
                    
                    return $atividade;
                });



                return response()->json($dashboard, 200);
            }

        }catch(\Exception $ex){
            return response($ex, 500);
        }
    }
}
