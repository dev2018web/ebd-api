<?php

namespace App\Http\Controllers;

use App\Models\Escola;
use App\Models\RL\UsuarioEscola;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EscolaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $escolas = Escola::where('nome_congregacao', 'like', "%" . $request->get("s") . "%")->paginate(10);
            return response()->json($escolas, 200);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = "";
            $status = 400;

            $validator = Validator::make($request->post(), [
                'nome_congregacao' => 'required',
                'gestor_id' => 'required|exists:App\Models\User,id',
                'endereco' => 'required',
            ]);

            if ($validator->fails()) {
                $data = $validator->errors();
            } else {
                $usuario = User::find($request->post('gestor_id'));

                if ($usuario->role == "GESTOR") {
                    $usuario->status = "ACTIVE";
                    $usuario->save();

                    $endereco = new EnderecoController();
                    $endereco_id = $endereco->create($request->post("endereco"));

                    if ($request->has('foto') && $request->file('foto')->isValid()) {
                        $path = $request->foto->store('images');

                        if ($endereco_id != -1) {
                            extract($request->post());

                            $data = [
                                'nome_congregacao' => $nome_congregacao,
                                'gestor_id' => $gestor_id,
                                'foto' => $path,
                                "endereco_id" => 1,
                            ];  

                            $data = Escola::create($data);
                            $status = 202;
                        } else {
                            $data = "Os dados do endereço estão incompletos";
                        }
                    }else{
                      $data = ["msg" => "Selecione uma imagem!"];
                    }
                } else {
                    $data = ["msg" => "Usuário selecionado não é um gestor"];
                }
            }

            return response()->json($data, $status);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Exibe uma escola específica
     *
     * @param  int $school_id
     * @return \Illuminate\Http\Response
     */
    public function show(int $school_id)
    {
        try {
            $escola = Escola::with(['endereco', 'alunos', 'classes.categoria', 'licoes', 'classes.alunos' => function ($query) {
                $query->where("usuarios.role", "ALUNO");
            }])->find($school_id);

            return response()->json($escola, 200);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Escolas  $escolas
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, int $school_id)
    {
        try {
            $escola = new Escola;

            $isFieldValid = $this->fillForUpdate($request->post(), $escola->fillableToUpdate);

            if ($isFieldValid->errors) {
                $data = $isFieldValid->message;
                $status = 400;
            } else {
                $escola::where(['id' => $school_id])->update($request->only('nome_congregacao'));
                $data = ['updated' => true];
                $status = 202;
            }

            return response()->json($data, $status);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $escola_id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $escola_id)
    {
        try {
            $escola = Escola::find($escola_id);

            if ($escola) {
                $escola->delete();
            } else {
                return response()->json(["error" => "escola já foi removida"], 400);
            }

            return response()->json($escola, 200);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    public function setStudentInSchool(Request $request)
    {
        try {
            $response = "";
            $status = 400;

            $validator = Validator::make($request->post(), [
                'id_usuario' => 'required|exists:App\Models\User,id',
                'id_school' => 'required|exists:App\Models\Escola,id',
            ]);

            if ($validator->fails()) {
                $response = $validator->errors();
            } else {
                extract($request->post());

                $response = User::isUserStudent($id_usuario);

                if (!$response) {
                    $data = [
                        'usuario_id' => $id_usuario,
                        'escola_id' => $id_school,
                    ];

                    if (!UsuarioEscola::where($data)->first()) {
                        UsuarioEscola::create($data);

                        $response = ['msg' => 'Aluno inserido na escola com sucesso!'];
                        $status = 202;
                    } else {
                        $response = ['error' => 'Esse aluno já faz parte dessa escola'];
                    }
                }
            }

            return response()->json($response, $status);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $escola_id
     * @return \Illuminate\Http\Response
     */
    public function removeStudentFromSchool(Request $request)
    {
        try {
            $response = "";
            $status = 400;

            $validator = Validator::make($request->post(), [
                'id_usuario' => 'required|exists:App\Models\User,id',
                'id_school' => 'required|exists:App\Models\Escola,id',
            ]);
            if ($validator->fails()) {
                $response = $validator->errors();
            } else {
                extract($request->post());

                if (UsuarioEscola::find($id_school)) {
                    $data = [
                        'escola_id' => $id_school,
                        'usuario_id' => $id_usuario,
                    ];

                    $escola_aluno = UsuarioEscola::where($data)->delete();

                    $response = ["msg" => "aluno removido da escola com sucesso!"];
                    $status = 200;
                } else {
                    $response = ["error" => "esse aluno não faz parte dessa escola"];
                }
            }

            return response()->json($response, $status);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Lista atividades dentro de uma aula que está dentro de uma escola
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function ActivitiesInAulaSchool(Request $request){
        try{            
            $validator = Validator::make($request->post(), [
                'aula_id' => 'required|exists:App\Models\Aula,id',
                'escola_id' => 'required|exists:App\Models\Escola,id',
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            } else {
                $ActivitiesInSchool = Escola::getActivitiesInAulaEscola(['aula_id'=>$request->post('aula_id'), 'escola_id'=>$request->post('escola_id')]);

                return response()->json($ActivitiesInSchool, 200);
            }
        }catch(\Exception $ex){
            return response($ex, 500);
        }
    }

    public function removeAtividadeFromEscola(Request $request)
    {
    }
}
