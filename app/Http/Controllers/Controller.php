<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function serverError(){
        $message = "Ocorreu um erro. Tente mais tarde ou entre em contato com o administrador";
        $status = 500;

        http_response_code($status);
        echo json_encode(["error" => $message]);die();
    }

    protected function fillForUpdate(array $dados, $fillableToUpdate)
    {
        $string = "Campo(s) ";
        $data = ['errors'=>true];
        $countFieldNoInArray = 0;

        foreach (array_keys($dados) as $dado) {
            $fieldInArray = in_array($dado, $fillableToUpdate) ? true : false;
            if (!$fieldInArray) {
                $string .= $dado . ',';
                $countFieldNoInArray ++;
            }
        }
        $string .= " não são válidos";

        if ($countFieldNoInArray > 0) {
            $data['message'] = $string;
        } else {
            $data['errors']=false;
            $data['message'] = 'Todos os dados são válidos';
        }

        return (object)$data;
    }
}
