<?php

namespace App\Http\Controllers;

use App\Models\Endereco;
use App\Models\Escola;
use App\Models\RL\UsuarioEscola;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard(Request $request)
    {
        try {
            $escolas = Escola::where('nome_congregacao', 'like', "%" . $request->get("s") . "%")->with(['endereco', 'alunos', 'classes.categoria', 'licoes.aulas.atividades', 'classes.alunos' => function ($query) {
                $query->where("role", "ALUNO");
            }])->paginate(10);

            return response()->json($escolas, 200);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $validator = Validator::make($request->post(), [
                'role' => 'required',
                'status' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            } else {
                $roles = ["ADMIN", "GESTOR", "PROFESSOR", "ALUNO"];
                if (in_array($request->post("role"), $roles)) {
                    $usuarios = User::select('id', 'nome', 'email', 'foto')->where(['role' => $request->post("role"), 'status' => $request->post("status")])->paginate(10);

                    return response()->json($usuarios, 200);
                } else {
                    return response()->json(["error" => "Tipo de usuário não encontrado"], 400);
                }
            }
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Cria uma novo usuário de um determinado tipo
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $message = "";
            $status = "";

            $validate_usuario = Validator::make($request->all(), [
                'nome' => 'required',
                'email' => 'required|unique:usuarios,email|email',
                'role' => 'required|in:ADMIN,GESTOR,PROFESSOR,ALUNO',
                'status' => 'required|in:DISABLED,ACTIVE',
                'data_nascimento' => 'date_format:"d-m-Y"|required|date'
            ]);
            
            $validate_endereco = "";
         
            if ($validate_usuario->fails()) {
                $message = [$validate_usuario->errors()];
                $status = 400;
            } else {
                $endereco = new EnderecoController();
                $endereco_id = null;

                if (!empty($request->post("endereco"))) {
                    $endereco_id = $endereco->create($request->post("endereco"));
                }

                $data = [
                    'nome' => $request->post('nome'),
                    'email' => $request->post('email'),
                    'role' => $request->post('role'),
                    'status' => $request->post('status'),
                    'data_nascimento' => date("Y-m-d", strtotime($request->post('data_nascimento'))),
                    'password' => Hash::make('12345'),
                    "endereco_id" => $endereco_id,
                ];

                if ($request->has('foto') && $request->file('foto')->isValid()) {
                    $data['foto'] = $request->foto->store('images');
                }

                if ($request->post('role') === 'PROFESSOR') {
                    $validate_usuario = Validator::make($request->post(), [
                        'escola_id' => 'required|exists:App\Models\Escola,id',
                        // 'classe_id' => 'required|exists:App\Models\Classe,id'
                    ]);
                    
                    if ($validate_usuario->fails()) {
                        $message = $validate_usuario->errors();
                        $status = 400;
                    } else {
                        $usuario = User::create($data);

                        $usuarioEscolaData = [
                            'usuario_id' => $usuario->id,
                            'escola_id' => $request->post('escola_id')
                        ];
                        $usuarioClasseData = [
                            'usuario_id' => $usuario->id,
                            'classe_id' => $request->post('classe_id'),
                        ];

                        Escola::insertUserInSchool($usuarioEscolaData);
                        // Escola::insertUserInClasse($usuarioClasseData);

                        $message = $usuario;
                        $status = 202;
                    }
                } else {
                    $message = User::create($data);
                    $status = 202;
                }
            }
            return response()->json($message, $status);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id_usuario)
    {
        try {
            $msg = "";
            $status = "";

            $usuario = User::with(['endereco'])->findOrFail($id_usuario);

            if ($usuario->status == $request->post('status') && $usuario->role == $request->post('role')) {
                $msg = $usuario;
                $status = 200;
            } else {
                $msg = ['error' => 'Os dados passados estão incorretos'];
                $status = 400;
            }
            return response()->json($msg, $status);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        try {
            $msg = "";
            $status = "";

            $validate_usuario = Validator::make($request->post(), [
                'nome' => 'required',
                'email' => 'required|unique:usuarios,email|email',
                'foto' => 'required',
                'role' => 'required|in:ADMIN,GESTOR,PROFESSOR,ALUNO',
                'status' => 'required|in:DESABLED,ACTIVE',
                'data_nascimento' => 'date_format:"d-m-Y"|required|date',
            ]);

            $validate_endereco = "";

            if ($request->post('endereco')) {
                $validate_endereco = Validator::make($request->post('endereco'), [
                    'cep' => 'required',
                    'logradouro' => 'required',
                    'complemento' => 'required',
                    'bairro' => 'required',
                    'localidade' => 'required',
                    'uf' => 'required',
                ]);
            }

            if ($validate_usuario->fails() || $validate_endereco->fails()) {
                $msg = [$validate_usuario->errors(), !empty($validate_endereco) ? $validate_endereco->errors() : "Você precisa passar o endereço do usuário"];
                $status = 400;
            } else {
                $usuario = User::findOrFail($id);

                if ($usuario->role == $request->post('role')) {
                    $data_endereco = new Endereco();
                    $data_endereco->find($usuario->endereco_id);
                    $data_endereco->update($request->post('endereco'));

                    $data_usuario = $request->post();
                    $data_usuario['data_nascimento'] = date("Y-m-d", strtotime($data_usuario['data_nascimento']));
                    unset($data_usuario['endereco']);

                    $usuario->update($data_usuario);

                    $msg = ['updated' => true];
                    $status = 202;
                } else {
                    $msg = ['error' => "O tipo de usuário não pode ser alterado"];
                    $status = 400;
                }
            }

            return response()->json($msg, $status);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id_usuario)
    {
        try {
            $msg = "";
            $status = "";

            $usuario = User::findOrFail($id_usuario);

            if ($usuario->status == $request->post('status') && $usuario->role == $request->post('role')) {
                $usuario->delete();

                $msg = ['deleted' => true];
                $status = 200;
            } else {
                $msg = ['error' => 'Os dados passados estão incorretos'];
                $status = 400;
            }
            return response()->json($msg, $status);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    public function updateProfile()
    {
        $user_logged = User::userLogged();
        
        return response($user_logged, 500);
    }
}
