<?php

namespace App\Http\Controllers;

use App\Models\Licao;
use App\Models\Escola;
use App\Models\RL\LicaoRL;
use App\Models\RL\LicaoClasse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LicaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $message = "";
        $status = 500;

        try {
            $licoes = Licao::with('categoria')->get();

            $message = $licoes;
            $status = 200;

            return response()->json(["data" => $message], $status);
        } catch (\Exception $ex) {
            $message = "Ocorreu um erro. Tente mais tarde ou entre em contato com o administrador";
            $status = 500;

            return response()->json(["error" => $message], $status);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validator = Validator::make($request->post(), [
                'titulo' => 'required',
                'categoria_id' => 'integer|required|exists:App\Models\Categoria,id',
            ]);

            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            }else{
                extract($request->post());

                $data = [
                    'titulo' => $titulo,
                    'categoria_id' => $categoria_id,
                ];

                $licao = Licao::create($data);
            }

            return response()->json($licao, 200);
        }catch(\Exception $ex){
            return response($ex, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Licao  $licao
     * @return \Illuminate\Http\Response
     */
    public function show(Licao $licao)
    {
        try{
            $data = "";
            $status = 400;

            $licao = $licao->with(['categoria', 'aulas'])->first();

            if($licao){
                $data = $licao;
                $status = 200;
            }else{
                $data = ['msg'=>'Lição não encontrada'];
                $status = 404;
            }

            return response()->json($data, $status);
        }catch(\Exception $ex){
            return response($ex, 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Licao  $licao
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, int $licao_id)
    {
        try{
            $validator = Validator::make($request->post(), [
                'titulo' => 'required',
                'categoria_id' => 'integer|required|exists:App\Models\Categoria,id',
            ]);

            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            }else{
                extract($request->post());

                $data = [
                    'titulo' => $titulo,
                    'categoria_id' => $categoria_id,
                ];

                $escolas = Licao::find($licao_id)->update($data);
            }

            return response()->json(['updated'=>true], 200);
        }catch(\Exception $ex){
            return response($ex, 500);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Licao  $licao
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $licao_id)
    {
        try{
            $licao = Licao::find($licao_id);

            if($licao){
                $licao->delete();
            }else{
                return response()->json(["error"=> "liçao já foi removida"], 404);
            }

            return response()->json($licao, 200);
        }catch(\Exception $ex){
            return response()->json($ex, 500);
        }
    }

    /**
     * Coloca uma lição em uma escola
     *
     * @param  \App\Models\Licao  $licao
     * @return \Illuminate\Http\Response
     */
    public function setLessonInSchool(Request $request)
    {
        try{
            $data = "";
            $status = 400;

            $validator = Validator::make($request->post(), [
                "id_licao" => 'integer|required|exists:App\Models\Licao,id',
                "id_escola" => 'integer|required|exists:App\Models\Escola,id'
            ]);

            if($validator->fails()){
                $data = $validator->errors();
            }else{
                extract($request->post());

                $data = [
                    'licao_id' => $id_licao,
                    'classe_id' => $id_escola,
                ];

                $licaoRl = LicaoRL::where($data)->first();

                if(!$licaoRl){
                    $licaoRl = LicaoRL::create($data);

                    $data = ['msg'=>'a lição foi adicionada à escola com sucesso!'];
                    $status = 202;
                }else{
                    $data = ['msg'=>'Essa lição já pertence a essa escola'];
                    $status = 400;
                }
            }

            return response()->json($data, $status);
        }catch(\Exception $ex){
            return response()->json($ex, 500);
        }
    }

    /**
     * Sets a lesson in a class
     *
     * @param  \App\Models\Licao  $licao
     * @return \Illuminate\Http\Response
     */
    public function setLessonInClass(Request $request)
    {
        try{

            $data = "";
            $status = 200;

            $validator = Validator::make($request->post(), [
                "id_licao" => 'integer|required|exists:App\Models\Licao,id',
                "id_classe" => 'integer|required|exists:App\Models\Classe,id'
            ]);

            if($validator->fails()){
                $data = $validator->errors();
            }else{
                extract($request->post());

                $data = [
                    'licao_id' => $id_licao,
                    'classe_id' => $id_classe,
                ];

                $licao_classe = LicaoClasse::where($data)->first();

                if(!$licao_classe){
                    $licao_classe = LicaoClasse::create($data);

                    $data = $licao_classe;
                    $status = 202;
                }else{
                    $data = ['msg'=>'Essa lição já pertence a essa classe'];
                    $status = 400;
                }
            }

            return response()->json($data, $status);
        }catch(\Exception $ex){
            return $ex;
        }
    }
}
