<?php

namespace App\Http\Controllers;

use App\Models\Aula;
use App\Models\Classe;
use App\Models\Conteudo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\RL\AulaConteudo;

class ConteudoController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $message = "";
            $status = 400;

            $validator = Validator::make($request->post(), [
                "aula_id"=> "required|exists:App\Models\Aula,id",
                "content"=> "required",
                "type"=> "required|in:VIDEO,TEXTO",
                'classe_id'=> "required|exists:App\Models\Classe,id"
            ]);

            if ($validator->fails()) {
                $message = $validator->errors();
            } else {
                extract($request->post());

                $licao_id = $aula = Aula::find($aula_id)->licao->id;

                if (Classe::isLicaoInClasse($licao_id, $classe_id)) {
                    if ($request->post('type')==='VIDEO') {
                        $youtube_url = "https://www.youtube.com/watch?v=";
                        $content = strpos($request->post('content'), $youtube_url) === 0 ? explode($youtube_url, $request->post('content'))[1] : '';
                    }

                    $data = [
                        'conteudo' => $content,
                        'tipo' => $type,
                        'autor_id' => User::userLogged()->id
                    ];

                    $conteudo_criado = Conteudo::create($data);

                    $data = [
                        'aula_id' => $aula_id,
                        'conteudo_id' => $conteudo_criado->id,
                    ];

                    $aula_conteudo = AulaConteudo::create($data);
                    $message = $conteudo_criado;
                    $status = 200;
                } else {
                    $message = ['error'=>"Ops! Você tentou inserir conteúdo em uma aula que não está dentro da classe"];
                    $status = 401;
                }
            }
            return response()->json($message, $status);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Conteudo  $conteudo
     * @return \Illuminate\Http\Response
     */
    public function show(Conteudo $conteudo)
    {
        try {
            return response()->json($conteudo, 200);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Conteudo  $conteudo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Conteudo $conteudo)
    {
        try {
            $validator = Validator::make($request->post(), [
                "content"=> "required"
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            } else {
                extract($request->post());

                $data = [
                    'conteudo' => $content
                ];

                $conteudo->update($data);
            }

            return response()->json($conteudo, 200);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Conteudo  $conteudo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Conteudo $conteudo)
    {
        try {
            $conteudo->delete();

            return response()->json($conteudo, 200);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }
}
