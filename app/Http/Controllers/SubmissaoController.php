<?php

namespace App\Http\Controllers;

use App\Models\Submissao;
use App\Models\Atividade;
use App\Models\User;
use App\Models\RL\UsuarioAtividade;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SubmissaoController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $message = "";
            $status = 400;

            $validator = Validator::make($request->all(), [
                "arquivo"=> "required|mimes:svg,jpeg,bmp,png,pdf,doc,epub,txt,ppt,eps",
                "atividade_id"=> "required|exists:App\Models\Atividade,id"
            ]);

            if ($validator->fails()) {
                $message = $validator->errors();
            } else {
                extract($request->post());
                $atividade = Atividade::find($atividade_id);

                if (!empty($atividade)) {
                    if ($atividade->tipo_atividade === 'SUB' && $request->file('arquivo')->isValid()) {
                        $path = $request->arquivo->store('submissoes');

                        $data = [
                            'atividade_id' => $atividade_id,
                            'caminho' => $path,
                            'usuario_id' => User::userLogged()->id
                        ];

                        $conteudo_criado = Submissao::create($data);
        
                        $message = $conteudo_criado;
                        $status = 200;
                    } else {
                        $message = ['error' => "Essa não é uma atividade de submissão"];
                    }
                } else {
                    $message = ['error' => "Atividade não existe"];
                }
            }
            return response()->json($message, $status);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Submissao  $submissao
     * @return \Illuminate\Http\Response
     */
    public function destroy(Submissao $submissao)
    {
        //
    }

    /**
     * Put note in a submission activity.
     *
     * @param int $usuario_id ,
     * @param int $atividade_id ,
     *
     * @return \Illuminate\Http\Response
     */
    public function putNoteInSubmissionActitivity(Request $request)
    {
        try {
            $validator = Validator::make($request->post(), [
                "usuario_id" => "required|exists:App\Models\User,id",
                "atividade_id" => "required|exists:App\Models\Atividade,id",
                'nota' => 'required'
            ]);

            $message = "";
            $status = 404;

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            } else {
                extract($request->post());

                $atividade = Atividade::find($atividade_id);

                if (!empty($atividade)) {
                    if ($atividade->tipo_atividade === "SUB") {
                        $dados = [
                            'usuario_id' => $usuario_id,
                            'atividade_id' => $atividade->id
                        ];

                        if (UsuarioAtividade::where($dados)->get()->count()) {
                            $message = ['error'=> 'Ops! Já foi cadastrada uma nota para essa atividade!'];
                            $status = 401;
                        } else {
                            $dados['nota'] = $nota;

                            $nota_atividade  = UsuarioAtividade::create($dados);

                            $message = ['nota'=>$nota_atividade->nota,'success' => true];
                            $status = 200;
                        }
                    } else {
                        $message = ['error'=> 'Ops! Essa atividade não é de submissão!'];
                        $status = 400;
                    }
                } else {
                    $message = ['error' => "Atividade não existe"];
                    $status = 400;
                }

                return response()->json($message, $status);
            }
        } catch (\Exception $ex) {
            return $ex;
        }
    }
}
