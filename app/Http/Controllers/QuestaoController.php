<?php

namespace App\Http\Controllers;

use App\Models\Atividade;
use App\Models\Questao;
use App\Models\User;
use App\Models\RL\UsuarioAlternativa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class QuestaoController extends Controller
{
    private $questao_escolhida;
    private $usuario_id;
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->post(), [
                'descricao' => 'required',
                'atividade_id' => 'required|exists:App\Models\Atividade,id',
                'multipla_escolha' => 'required|in:0,1'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            } else {
                $atividade = Atividade::find($request->post('atividade_id'));

                if (!empty($atividade) && $atividade->tipo_atividade === "QUEST") {
                    extract($request->post());

                    $data = [
                        'descricao' => $descricao,
                        'atividade_id' => $atividade_id,
                        'multipla_escolha' => $multipla_escolha
                    ];

                    $questao = Questao::create($data);

                    return response()->json($questao, 200);
                } else {
                    return response()->json(['error'=>'Ops! Nessa atividade não pode ser adicionado um questionário.'], 200);
                }
            }
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Questao  $questao
     * @return \Illuminate\Http\Response
     */
    public function show(Questao $questao)
    {
        try {
            return response()->json($questao->with('alternativas')->find($questao->id), 200);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Questao  $questao
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Questao $questao)
    {
        try {
            $validator = Validator::make($request->post(), [
                'descricao' => 'required',
                'multipla_escolha' => 'required|in:0,1'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            } else {
                extract($request->post());

                $data = [
                    'descricao' => $descricao,
                    'multipla_escolha' => $multipla_escolha,
                ];

                $escolas = $questao->update($data);
            }

            return response()->json($questao, 200);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Questao  $questao
     * @return \Illuminate\Http\Response
     */
    public function destroy(Questao $questao)
    {
        try {
            $questao->delete();

            return response()->json($questao, 200);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
    * Answer a question
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function answerQuestion(Request $request)
    {
        try {
            $this->usuario_id = User::userLogged()->id;

            foreach ($request->post() as $check) {
                extract($check);

                $this->questao_escolhida = Questao::with('alternativas')->find($questao_id);

                if (!$this->wasQuestionAnswered($this->usuario_id)) {
                    $alternativa_marcada = $this->alternativeBelongsToQuestion($alternativa_id);

                    $is_multiple = count($alternativa_marcada) > 1 ? $this->isQuestionMultipleChoice() : true;

                    if ($is_multiple) {
                        if (count($alternativa_marcada) > 0) {

                            if (!$this->getEqualAnswer($this->usuario_id, $alternativa_id)) {
                                $usuario_id = $this->usuario_id;

                                $data = collect($alternativa_id)->map(function ($alternativa_id) use ($usuario_id) {
                                    return ['alternativa_id'=>$alternativa_id, 'usuario_id'=>$usuario_id];
                                });

                                UsuarioAlternativa::insert($data->toArray());
                            } else {
                                return response()->json(['error'=> 'Ops! Você já marcou essa alternativa'], 400);
                            }
                        } else {
                            return response()->json(['error'=> 'Ops! Você selecionou uma ou mais alternativas que não fazem parte da questão'], 400);
                        }
                    } else {
                        return response()->json(['error'=> 'Ops! Essa questão não é de múltipla escolha. Selecione apenas uma alternativa.'], 400);
                    }
                } else {
                    return response()->json(['error'=> 'Ops! Você já respondeu essa questão'], 400);
                }
            }

            return response()->json(['respondida' => true], 200);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /*
    * Verifica se a questão é de múltipla escolha.
    */
    private function isQuestionMultipleChoice()
    {
        return $this->questao_escolhida->multipla_escolha === 1;
    }

    /*
    * retorna se o usuário está tentando cadastrar essa questão pela segunda vez
    */
    private function getEqualAnswer(int $usuario_id, $ids)
    {   
        $alternativas_ids = is_integer($ids) ? [$ids] : $ids;

        $equal_answer = UsuarioAlternativa::whereIn('alternativa_id', $alternativas_ids)->where(['usuario_id' => $usuario_id])->get();

        return false;
        return count($equal_answer) > 0;
    }

    /*
    * Verifica se o a questão já foi respondidad
    */
    private function wasQuestionAnswered(int $usuario_id)
    {

        $alternativas_ids = ($this->questao_escolhida) ? $this->questao_escolhida->alternativas->map(function ($alternativa) {
            return $alternativa->id;
        }) : [];

        $asweredQuestionsByUser = UsuarioAlternativa::whereIn('alternativa_id', $alternativas_ids)->where(['usuario_id' => $usuario_id])->get();

        // Depois preciso corrigir aqui
        return false;
        return count($asweredQuestionsByUser) > 0;
    }

    /*
    * Verifica se uma ou mais alternativas pertencem a uma questão
    */
    private function alternativeBelongsToQuestion($ids)
    {   
        $alternativas_ids = is_integer($ids) ? [$ids] : $ids;

        $alternativas_marcada = ($this->questao_escolhida) ? $this->questao_escolhida->alternativas->filter(function ($alternativa) use ($alternativas_ids) {
            return collect($alternativas_ids)->some(function ($alternativa_id) use ($alternativa) {
                return $alternativa_id == $alternativa->id;
            });
        }) : [];

        return count($alternativas_marcada) === count($alternativas_ids) ? $alternativas_marcada : [];
    }
}
