<?php

namespace App\Http\Controllers;

use App\Models\Aula;
use App\Models\Classe;
use App\Models\RL\AulaRL;
use App\Models\RL\AtividadeRL;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AulaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $data = "";
            $status = 200;

            $data = Aula::with('atividades')->get();

            return response()->json(["data" => $data], $status);
        } catch (\Exception $ex) {
            return response()->json(["error" => $ex], 500);
        }
    }

    /**
     * Cria uma novo usuário de um determinado tipo
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $message = "";
            $status = 400;

            $validator = Validator::make($request->post(), [
                "licao_id"=> "required|exists:App\Models\Licao,id",
                "titulo"=> "required|max:255"
            ]);

            if ($validator->fails()) {
                $message = $validator->errors();
            } else {
                extract($request->post());

                $data = [
                    'licao_id' => $licao_id,
                    'titulo' => $titulo
                ];

                $aula = Aula::create($data);

                $message = $aula;
                $status = 202;
            }

            return response()->json($message, $status);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Aula  $Aula
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $aula_id)
    {
        try {
            $message = "";
            $status = "";

            $aula = Aula::with('atividades')->findOrFail($aula_id);

            $message = $aula;
            $status = 200;
            return response()->json($message, $status);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Aula  $Aula
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        try {
            $message = "";
            $status = "";

            $validate = Validator::make($request->post(), [
                    "classe_id"=> "required|integer",
                    "tipo"=> "in:0,1"
                ]);

            if ($validate->fails()) {
                $message = $validate->errors();
                $status = 400;
            } else {
                extract($request->post());
                $classe = Classe::find($classe_id);

                if ($classe) {
                    $aula = Aula::findOrFail($id);

                    $data_aula = $request->post();

                    $aula->update($data_aula);

                    $dataRL = [
                        'classe_id' => $classe_id,
                        'aula_id' => $aula->id
                    ];

                    $aula_classe = AulaRL::where($dataRL)->update($dataRL);

                    $message = "Aula atualida com sucesso!";
                    $status = 202;
                } else {
                    $message = "A classe que você deseja colocar essa aula não existe";
                    $status = 400;
                }
            }
            return response()->json(["msg"=>$message], $status);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
    * Remove the specified resource from storage.
     *
     * @param  \App\Models\Aula  $Aula
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $aula_id)
    {
        try {
            $message = "";
            $status = "";

            $aula = Aula::findOrFail($aula_id);

            $aula->delete();

            $message = ['deleted'=>true];
            $status = 200;

            return response()->json($message, $status);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Adiciona uma atviidade em uma aula.
     *
     * @param  int $escola_id
     * @param  int $aula_id
     * @param  int $atividade_id
     * @return \Illuminate\Http\Response
     */
    public function setAtividadeInAula(Request $request)
    {
        try{
            $response = "";
            $status = 200;

            $validator = Validator::make($request->post(), [
                'escola_id' => 'required|exists:App\Models\Escola,id',
                'atividade_id' => 'required|exists:App\Models\Atividade,id',
                'aula_id' => 'required|exists:App\Models\Aula,id',
            ]);


            if ($validator->fails()) {
                $status = 400;
                $response = $validator->errors();
            } else {
                extract($request->post());

                $dataRL = [
                    'aula_id' => $aula_id,
                    'atividade_id' => $atividade_id,
                    'escola_id' => $escola_id
                ];


                if(!AtividadeRL::where($dataRL)->first()){
                    $response = AtividadeRL::create($dataRL);
                }else{
                    $response = ['msg'=>'Essa lição já faz parte dessa aula e/ou escola'];
                    $status = 400;
                }
            }
            return response()->json($response, $status);
        }catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    public function removeAtividadeFromAula(Request $request){
        try{
            $response = "";
            $status = 200;

            $validator = Validator::make($request->post(), [
                'atividade_id' => 'required|exists:App\Models\Atividade,id',
                'aula_id' => 'required|exists:App\Models\Aula,id',
            ]);

            if ($validator->fails()) {
                $status = 400;
                $response = $validator->errors();
            } else {
                extract($request->post());

                $dataRL = [
                    'aula_id' => $aula_id,
                    'atividade_id' => $atividade_id,
                    'escola_id' => !empty($escola_id) ? $escola_id : null
                ];

                if(AtividadeRL::where($dataRL)->first()){
                    AtividadeRL::where($dataRL)->delete();
                    $response = ['deleted'=>true];
                }else{
                    $response = ['msg'=>'Essa lição não faz parte dessa aula e/ou escola'];
                    $status = 400;
                }
            }
            return response()->json($response, $status);
        }catch (\Exception $ex) {
            return response($ex, 500);
        }
    }
}
