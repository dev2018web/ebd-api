<?php

namespace App\Http\Controllers;

use App\Models\Atividade;
use App\Models\Questao;
use App\Models\Submissao;
use App\Models\Aula;
use App\Models\Classe;
use App\Models\RL\AtividadeRL;
use App\Models\RL\UsuarioAlternativa;
use App\Models\RL\UsuarioNotaQuestao;
use App\Models\RL\UsuarioAtividade;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use NotificationChannels\ExpoPushNotifications\Models\Interest;

class AtividadeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $data = "";
            $status = 200;

            $data = Atividade::get();

            return response()->json(["data" => $data], $status);
        } catch (\Exception $ex) {
            return response()->json(["error" => $ex], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $message = "";
            $status = 202;

            $usuario_logado = User::userLogged();

            $validator = Validator::make($request->post(), [
                "aula_id" => "required|exists:App\Models\Aula,id",
                "titulo" => "required|max:255",
                "escola_id" => Rule::requiredIf($usuario_logado->role === 'ADMIN'),
                "tipo_atividade" => "required|in:SUB,QUEST",
                "classe_id" => "required|exists:App\Models\Classe,id",
            ]);

            if ($validator->fails()) {
                $message = $validator->errors();
                $status = 400;
            } else {
                extract($request->post());

                $licao_id = $aula = Aula::find($aula_id)->licao->id;

                if (Classe::isLicaoInClasse($licao_id, $classe_id)) {
                    // prepara os dados da atividade
                    $data_atividade = [
                        'titulo' => $titulo,
                        'tipo_atividade' => $tipo_atividade,
                        'autor_id' => $usuario_logado->id,
                        'senha' => substr(rand(), 0, 4)
                    ];

                    $atividade = Atividade::create($data_atividade);

                    $data_atividade_rl = [
                        'aula_id' => $aula_id,
                        'atividade_id' => $atividade->id
                    ];

                    // Pega o ID da escola baseado no tipo de usuário
                    switch (User::userLogged()->role) {
                    case 'ADMIN':
                        $data_atividade_rl['escola_id'] = $escola_id;
                        break;
                    case 'GESTOR':
                        // $escola_user = User::find(User::userLogged()->id)->escola();
                        // dd($escola_user);
                        // $escola_user_id = $escola_user ? $escola_user->id : 0;
                        break;
                    }

                    $aula_atividade_classe = Classe::putActivityInClass(['atividade_id'=>$atividade->id, 'classe_id'=>$classe_id, 'aula_id'=>$aula_id]);
                    $aula_atividade_escola = AtividadeRL::create($data_atividade_rl);


                    $message = $atividade;
                    $status = 200;
                } else {
                    $message = ['error'=>"Ops! Você tentou inserir atividade em uma aula que não está dentro da classe"];
                    $status = 401;
                }
            }
            return response()->json($message, $status);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Atividade $atividade
     * @return \Illuminate\Http\Response
     */
    public function show(Atividade $atividade)
    {
        try {
            $user_logged_id = User::userLogged()->id;

            return response()->json($atividade
                ->with('questoes.alternativas')
                ->with(['nota' => function ($query) use ($user_logged_id) {
                    $query->where('usuario_id', $user_logged_id);
                }])
                ->with(['questoes.nota' => function ($query) use ($user_logged_id) {
                    $query->where('usuario_id', $user_logged_id);
                }])->where('id', $atividade->id)
                ->first(), 200);
        } catch (\Exception $ex) {
            return $ex;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Atividade $atividade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Atividade $atividade)
    {
        try {
            $validator = Validator::make($request->post(), [
                "titulo" => "required|max:255",
            ]);
            $message = "";
            $status = 400;

            if ($validator->fails()) {
                $message = $validator->errors();
                $status = 400;
            } else {
                extract($request->post());

                $message = $atividade->update(['titulo' => $titulo]);
                $status = 202;
            }

            return response()->json($atividade, $status);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Atividade $atividade
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Atividade $atividade)
    {
        try {
            $validator = Validator::make($request->post(), [
                "aula_id" => "required|exists:App\Models\Aula,id",
                "escola_id" => "required|exists:App\Models\Escola,id"
            ]);

            $message = "";
            $status = 404;

            if ($validator->fails()) {
                $message = $validator->errors();
                $status = 400;
            } else {
                extract($request->post());

                $atividade->delete();
                AtividadeRL::where(['escola_id' => $escola_id, 'aula_id' => $aula_id, 'atividade_id' => $atividade->id])->delete();
                Questao::where(['atividade_id' => $atividade->id])->delete();
                Submissao::where(['atividade_id' => $atividade->id])->delete();

                $message = ['deleted' => true];
                $status = 200;
            }
            return response()->json($message, $status);
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }

    /**
     * Deliver the student activity when he finishes that
     *
     * @param int $usuario_id ,
     * @param int $atividade_id ,
     *
     * @return \Illuminate\Http\Response
     */
    public function deliverActivity(Request $request)
    {
        try {
            $validator = Validator::make($request->post(), [
                "atividade_id" => "required|exists:App\Models\Atividade,id"
            ]);

            $message = "";
            $status = 404;

            if ($validator->fails()) {
                $message = $validator->errors();
                $status = 400;
            } else {
                extract($request->post());

                $atividade = Atividade::with('questoes.alternativas')->find($atividade_id);
                $usuario_id = User::userLogged()->id;
 
                if (!empty($atividade)) {
                    $question_max_note = 10 / count($atividade->questoes);

                    $usuario_atividade = UsuarioAtividade::where(['usuario_id' => $usuario_id, 'atividade_id' => $atividade_id])->first();

                    if (empty($usuario_atividade)) {
                        // Calcula as notas de um aluno
                        $student_notes = $atividade->questoes->map(function ($questao) use ($usuario_id, $question_max_note) {
                            $alternativas_marcadas = $questao->alternativas->filter(function ($alternativa) use ($usuario_id) {
                                $usuario_alternativa = UsuarioAlternativa::where(['usuario_id' => $usuario_id, 'alternativa_id' => $alternativa->id])->first();
                                return $usuario_alternativa;
                            });

                            $alternativas_corretas = $questao->alternativas->filter(function ($alternativa) {
                                return $alternativa->correta === 1;
                            });

                            $alternativas_marcadas_corretas = $alternativas_marcadas->filter(function ($alternativa_marcada) {
                                return $alternativa_marcada->correta === 1;
                            });

                            if (count($alternativas_corretas) === 0) {
                                return ['usuario_id' => $usuario_id, 'questao_id' => $questao->id, 'nota'=>$question_max_note];
                            }

                            // Regra de três para calcular a nota de cada questão
                            $nota_questao = (count($alternativas_marcadas_corretas) * $question_max_note) / count($alternativas_corretas);

                            return ['usuario_id' => $usuario_id, 'questao_id' => $questao->id, 'nota' => $nota_questao];
                        });

                        $activity_note = array_sum($student_notes->map(function ($data) {
                            return $data['nota'];
                        })->toArray());
                    } else {
                        return response()->json(['error' => 'Ops! Você já entregou essa atividade'], 400);
                    }
                } else {
                    return response()->json(['error' => 'Ops! Essa atividade foi apagada!'], 400);
                }

                UsuarioAtividade::create(['usuario_id' => $usuario_id, 'atividade_id' => $atividade_id, 'nota' => $activity_note]);
                UsuarioNotaQuestao::insert($student_notes->toArray());

                return response()->json(["msg" => "Atividade entregue com sussesso!", "nota"=>$activity_note], 200);
            }
        } catch (\Exception $ex) {
            return response($ex, 500);
        }
    }


    public function notifyStudentAboutActivity()
    {
        $invoice = Interest::all();

        $data = $invoice->map(function ($item) {
            $user_name = User::find($item->key[strlen($item->key)-1])->nome;
            $first_name = explode(' ', $user_name)[0];

            return  [
                "to"=>$item->value,
                "title"=> "Novidade!",
                "body"=> $first_name . ", uma nova atividade foi publicada. Clique aqui e acesse a tela para fazer download.",
                "sound"=> "default",
            ];
        });

        $requestString = "";

        foreach ($data->toArray() as $item) {
            $options = [
                CURLOPT_URL => 'https://exp.host/--/api/v2/push/send',
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => \http_build_query($item)
            ];
    
            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $response = curl_exec($ch);
            curl_close($ch);


            echo response($response);
        }
    }

    /**
     * List the student activity in the class
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function listActivitiesOfStudentInClass(Request $request): object
    {
        try {
            $message = "";
            $status = 200;

            $loggedUser = User::userLogged();

            if (!$request->get('class_id')) {
                $message = ["Error"=>"Você precisa passar o campo class_id"];
                $status = 400;
            } else {
                $user_activities = UsuarioAtividade::where('usuario_id', '=', $loggedUser->id)->get();
                $activities_ids = $user_activities->map(fn ($user_activitie) => $user_activitie->atividade_id);

                $classe = Classe::where('id', $request->get('class_id'))->with(['atividades' => function ($query) use ($activities_ids) {
                    return $query->whereIn('id', $activities_ids);
                }])->first();
                
                return response()->json($classe->atividades, 200);
            }
            return response()->json(["data" => $message], $status);
        } catch (\Exception $ex) {
            return response()->json(["error" => $ex], 500);
        }
    }


    /**
     * Show a corrected activity
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function showCorrectedActivity(Request $request)
    {
        try {
            $message = "";
            $status = 200;

            $loggedUser = User::userLogged();

            if (!$request->get('activity_id')) {
                return response()->json(["Error"=>"Você precisa passar o campo activity_id"], 400);
            } else {
                $activity = Atividade::where('id', $request->get('activity_id'))->with(['nota' => function($query) use ($loggedUser){
                    return $query->where('usuario_id', $loggedUser->id);
                }])->with('questoes.alternativas')->first();
                $alternative_id = "";
    
                $user_alternatives = [];
    
                foreach ($activity->questoes as $questao) {
                    $alternative_ids = $questao->alternativas->map(fn ($alternative) => $alternative->id);
                    $user_alternatives[] = UsuarioAlternativa::where('usuario_id', $loggedUser->id)->whereIn('alternativa_id', $alternative_ids)->get();
                    
                    $lastInsertedUserAlternative = end($user_alternatives)->toArray();
                    
                    if (count($lastInsertedUserAlternative) > 0) {
                        $checkedQuestion = $questao->alternativas->filter(fn ($alternative) => in_array($alternative->id, $lastInsertedUserAlternative[0]))->toArray();
                        $questao->alternativa_marcada  = end($checkedQuestion);
                    }
                }

                return response()->json(["data" => $activity], 200);
            }
        } catch (\Exception $ex) {
            return response()->json(["error" => $ex], 500);
        }
    }
}
