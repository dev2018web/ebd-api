<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Conteudo extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'aula_id',
        'conteudo',
        'tipo',
        'autor_id'
    ];
}
