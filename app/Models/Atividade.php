<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Atividade extends Model
{
    use SoftDeletes;

    protected $table = "atividades";

    protected $fillable = [
        'titulo',
        'autor_id',
        'tipo_atividade',
        'senha'
    ];

    public function questoes()
    {
        return $this->hasMany('App\Models\Questao');
    }

    /*
    * pega a nota de uma atividade
    */
    public function nota(){
        return $this->hasOne('App\Models\RL\UsuarioAtividade');
    }
}
