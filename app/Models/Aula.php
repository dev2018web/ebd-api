<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Aula extends Model
{
    use SoftDeletes;

    protected $table = "aulas";

    protected $fillable = [
        'licao_id',
        'titulo'
    ];

    public function atividades()
    {
        return $this->belongsToMany('App\Models\Atividade', 'aula_atividade_escola');
    }

    public function licao()
    {
        return $this->belongsTo('App\Models\Licao');
    }
}
