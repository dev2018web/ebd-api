<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

class Escola extends Model
{
    use SoftDeletes;

    protected $table = "escolas";

    protected $fillable = [
        "nome_congregacao",
        "gestor_id",
        "endereco_id",
        'foto'
    ];

    public static function getActivitiesInAulaEscola(array $data): Collection
    {   
        $activities_ids = DB::table('aula_atividade_escola')->select('atividade_id')->where($data)->get();

        $ids = $activities_ids->map(function($item){
            return $item->atividade_id;
        });

        
        $atividades = DB::table('atividades')->whereIn('id', $ids)->get();        

        return $atividades;
    }

    public static function insertUserInClasse(array $data):void
    {
        DB::table('usuario_classe')->insert($data);
    }

    public static function insertUserInSchool(array $data):void
    {
        DB::table('usuario_escola')->insert($data);
    }
    
    // public function licoes()
    // {
    //     return $this->morphToMany('App\Models\Licao', 'licao_escola', 'licao_escola', 'licao_id');
    // }

    public function licoes()
    {
        return $this->belongsToMany('App\Models\Licao', 'licao_escola');
    }

    public function classes()
    {
        return $this->hasMany('App\Models\Classe');
    }

    public function classe()
    {
        return $this->hasOne('App\Models\Classe');
    }

    public function endereco()
    {
        return $this->hasOne('App\Models\Endereco', "id", "id_endereco");
    }

    public function alunos()
    {
        return $this->belongsToMany('App\Models\User', 'usuario_escola', 'escola_id', 'usuario_id');
    }
}
