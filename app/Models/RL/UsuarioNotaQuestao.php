<?php

namespace App\Models\RL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsuarioNotaQuestao extends Model
{
    use SoftDeletes;

    protected $table = "usuario_nota_questao";
    public $timestamps = false;

    protected $fillable = [
        'usuario_id',
        'questao_id',
        'nota'
    ];
}
