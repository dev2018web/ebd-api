<?php

namespace App\Models\RL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsuarioEscola extends Model
{
    use SoftDeletes;

    protected $table = "usuario_escola";
    public $timestamps = false;

    protected $fillable = [
        'escola_id',
        'usuario_id'
    ];
}
