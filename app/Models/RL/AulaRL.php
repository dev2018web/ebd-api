<?php

namespace App\Models\RL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AulaRL extends Model
{
    use SoftDeletes;

    protected $table = "aula_classe";
    public $timestamps = false;

    protected $fillable = [
        "aula_id",
        "classe_id"
    ];
}
