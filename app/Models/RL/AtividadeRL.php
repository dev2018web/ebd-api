<?php

namespace App\Models\RL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AtividadeRL extends Model
{
    protected $table = "aula_atividade_escola";
    public $timestamps = false;

    protected $fillable = [
        "aula_id",
        "atividade_id",
        "escola_id"
    ];
}
