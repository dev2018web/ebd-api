<?php

namespace App\Models\RL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsuarioClasse extends Model
{
    use SoftDeletes;

    protected $table = "usuario_classe";
    public $timestamps = false;

    protected $fillable = [
        "usuario_id",
        "classe_id",
        "role"
    ];
}
