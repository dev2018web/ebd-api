<?php

namespace App\Models\RL;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class LicaoRL extends Model
{
    use SoftDeletes;

    protected $table = "licao_escola";
    public $timestamps = false;

    protected $fillable = [
        "licao_id",
        "escola_id"
    ];


}
