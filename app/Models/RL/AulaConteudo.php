<?php

namespace App\Models\RL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AulaConteudo extends Model
{
    protected $table = "aula_conteudo";
    public $timestamps = false;

    protected $fillable = [
        'aula_id',
        'conteudo_id'
    ];
}
