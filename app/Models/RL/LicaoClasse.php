<?php

namespace App\Models\RL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LicaoClasse extends Model
{
    use SoftDeletes;

    protected $table = "licao_classe";

    protected $fillable = [
        "licao_id",
        "classe_id"
    ];

}
