<?php

namespace App\Models\RL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsuarioAtividade extends Model
{
    protected $table = "usuario_atividade";
    public $timestamps = false;

    protected $fillable = [
        'usuario_id',
        'atividade_id',
        'nota'
    ];
}
