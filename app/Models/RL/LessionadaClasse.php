<?php

namespace App\Models\RL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LessionadaClasse extends Model
{
    protected $table = "lessionada_classe";
    public $timestamps = false;

    protected $fillable = [
        'classe_id',
        'aula_id'
    ];
}
