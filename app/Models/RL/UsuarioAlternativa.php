<?php

namespace App\Models\RL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsuarioAlternativa extends Model
{
    use SoftDeletes;

    protected $table = "usuario_alternativa";
    public $timestamps = false;

    protected $fillable = [
        "usuario_id",
        "alternativa_id"
    ];


}
