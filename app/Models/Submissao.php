<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Submissao extends Model
{
    use SoftDeletes;

    protected $table = "submissoes";

    protected $fillable = [
        'atividade_id',
        'caminho',
        'usuario_id'
    ];

    public function atividade(){
        return $this->belongsTo('App\Models\Atividade', 'atividade_id');
    }
}
