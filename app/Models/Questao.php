<?php

namespace App\Models;       

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Questao extends Model
{
    use SoftDeletes;

    protected $table = "questoes";

    protected $fillable = [
        "descricao",
        "atividade_id",
        "multipla_escolha"
    ];

    public function alternativas()
    {
        return $this->hasMany('App\Models\Alternativa');
    }

    public function nota(){
        return $this->hasOne('App\Models\RL\UsuarioNotaQuestao');
    }    
}
