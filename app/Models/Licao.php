<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Licao extends Model
{
    use SoftDeletes;

    protected $table = "licoes";

    protected $fillable = [
        "titulo",
        "categoria_id"
    ];

    public function categoria(){
        return $this->hasOne('App\Models\Categoria', 'id');
    }

    public function escolas(){
        return $this->belongsToMany('App\Models\Escola', 'licao_escola', 'licao_id');
    }

    public function aulas(){
        return $this->hasMany('App\Models\Aula');
    }

}
