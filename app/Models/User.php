<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use SoftDeletes;
    use Notifiable;

    protected $table = "usuarios";

    protected $fillable = [
        'nome',
        'email',
        'foto',
        'role',
        'status',
        'password',
        'data_nascimento',
        'id_endereco'
    ];

    public static function isUserStudent($id_user){
        $user = User::where('id', $id_user)->first();
        return $user->role == 'ALUNO' ? '' : ['error' => 'Esse usuário não é um aluno!'];
    }

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public static function userLogged()
    {
        return auth('api')->user();
    }


    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function pontuations() {
        return $this->hasMany('App\Models\RL\UsuarioAtividade', 'usuario_id');
    }


    public function escola(){
        return $this->belongsTo('App\Models\Escola');
    }

    public function classe(){
        return $this->belongsTo('App\Models\RL\UsuarioClasse', 'id', 'usuario_id');
    }

    public function endereco(){
        return $this->hasOne('App\Models\Endereco', "id", "id_endereco");
    }
}
