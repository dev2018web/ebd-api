<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

class Classe extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'titulo',
        'id_classe_categoria',
        'escola_id',
        'nummax_professores',
        'foto',
    ];

    // Verifica se uma lição está dentro da classe
    public static function isLicaoInClasse($licao_id, $classe_id)
    {
        return count(DB::table('licao_classe')->where(['licao_id'=>$licao_id, 'classe_id'=>$classe_id])->get()->toArray()) > 0;
    }

    public static function putActivityInClass(array $data):void
    {
        DB::table('aula_atividade_classe')->insert($data);
    }

    /*
    * Lista o id das atividades
    */
    private static function ativitiesIds(array $data)
    {
        $activities_ids = DB::table('aula_atividade_classe')->select('atividade_id')->where($data)->get();

        $ids = $activities_ids->map(function ($item) {
            return $item->atividade_id;
        });

        return $ids;
    }

    /*
    * Pega atividades dentro de uma classe
    */
    public static function getActivitiesInAulaClass(array $data): Collection
    {
        $atividade_ids = self::ativitiesIds($data);
        $atividades = DB::table('atividades')->whereIn('id', $atividade_ids)->get();

        return $atividades;
    }

    /*
    * Pega atividades dentro de uma classe e relaciona com as notas
    */
    public static function getActivitiesInClassWithNote(array $data, $user_id)
    {
        $atividade_ids = self::ativitiesIds($data);

        $atividades = DB::table('atividades')
        ->select('*')
        ->whereIn('id', $atividade_ids)
        ->leftJoin('usuario_atividade', function ($join) use ($user_id) {
            $join->on('atividade_id', '=', 'id')
            ->where('usuario_id', $user_id);
        })
        ->get();

        // remove os registros duplicados
        $atividades_unique = $atividades->unique();

        return $atividades_unique;
    }

    public function escola()
    {
        return $this->belongsTo('App\Models\Escola');
    }

    public function atividades()
    {
        return $this->belongsToMany('App\Models\Atividade', 'aula_atividade_classe', 'classe_id', 'atividade_id');
    }

    public function alunos()
    {
        return $this->belongsToMany('App\Models\User', 'usuario_classe', 'classe_id', 'usuario_id');
    }

    // public function licoes()
    // {
    //     return $this->belongsTomany('App\Models\Aula');
    // }

    public function licoes()
    {
        return $this->belongsToMany('App\Models\Licao', 'licao_classe', 'licao_id', 'classe_id');
    }

    public function categoria()
    {
        return $this->hasOne('App\Models\Categoria', 'id');
    }
}
