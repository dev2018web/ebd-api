<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Alternativa extends Model
{
    use SoftDeletes;

    protected $table = "alternativas";

    protected $fillable = [
        "texto",
        "questao_id",
        "correta",
    ];

    public function questao(){
        return $this->belongsTo('App\Models\Questao');
    }

}
