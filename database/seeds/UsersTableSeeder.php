<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Endereco;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $endereco = Endereco::create([
            'cep' => '54300-100',
            'logradouro' => 'Rua capitão ramos',
            'bairro' => 'Zumbi do Pacheco',
            'localidade' => 'Jabotão dos Guararapes',
            'complemento' => 'Casa',
            'uf' => 'PE'
        ]);

        $user = User::create([
            'nome' => 'Paulo',
            'email' => 'pr838908@gmail.com',
            'foto' => 'teste.png',
            'password' => Hash::make('12345'),
            'role' => 'ADMIN',
            'status' => 'ACTIVE',
            'data_nascimento' => '1999-09-02',
            'endereco_id' => $endereco->id
        ]);
    }
}
