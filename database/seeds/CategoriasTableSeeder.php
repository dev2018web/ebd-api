<?php

use Illuminate\Database\Seeder;
use App\Models\Categoria;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ["Juvenis", "Adultos", "Idosos"];

        foreach($data as $item){
            $categoria = Categoria::create(['nome'=>$item]);
        };
    }
}
