<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLicoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('licoes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titulo');
            $table->unsignedBigInteger('categoria_id');
            $table->foreign('categoria_id')->references('id')->on('categorias');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('licao_escola', function (Blueprint $table) {
            $table->unsignedBigInteger('licao_id');
            $table->unsignedBigInteger('escola_id');
            $table->foreign('licao_id')->references('id')->on('licoes');
            $table->foreign('escola_id')->references('id')->on('escolas');
            $table->softDeletes();
        });

        Schema::create('licao_classe', function (Blueprint $table) {
            $table->unsignedBigInteger('licao_id');
            $table->unsignedBigInteger('classe_id');
            $table->foreign('licao_id')->references('id')->on('licoes');
            $table->foreign('classe_id')->references('id')->on('classes');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {      
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('licoes');
        Schema::dropIfExists('licao_escola');
        Schema::dropIfExists('licao_classe');
    }
}
