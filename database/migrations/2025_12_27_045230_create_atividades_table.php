<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAtividadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atividades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titulo');
            $table->text('descricao')->nullable();
            $table->text('senha');
            $table->unsignedBigInteger('autor_id');
            $table->foreign('autor_id')->references('id')->on('usuarios');
            $table->enum('tipo_atividade', ['SUB', 'QUEST']);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('aula_atividade_escola', function (Blueprint $table) {
            $table->unsignedBigInteger('aula_id');
            $table->unsignedBigInteger('atividade_id');
            $table->unsignedBigInteger('escola_id')->nullable();
            $table->enum('status', ['MASTER', 'GESTOR', 'PROFESSOR']);
            $table->foreign('aula_id')->references('id')->on('aulas');
            $table->foreign('atividade_id')->references('id')->on('atividades');
            $table->foreign('escola_id')->references('id')->on('escolas');
        });

        Schema::create('aula_atividade_classe', function (Blueprint $table) {
            $table->unsignedBigInteger('aula_id');
            $table->unsignedBigInteger('atividade_id');
            $table->unsignedBigInteger('classe_id');
            $table->foreign('aula_id')->references('id')->on('aulas');
            $table->foreign('atividade_id')->references('id')->on('atividades');
            $table->foreign('classe_id')->references('id')->on('classes');
        });

        Schema::create('usuario_atividade', function (Blueprint $table) {
            $table->unsignedBigInteger('usuario_id');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
            $table->unsignedBigInteger('atividade_id');
            $table->foreign('atividade_id')->references('id')->on('atividades');
            $table->float('nota');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('atividades');
        Schema::dropIfExists('atividade_aula');
        Schema::dropIfExists('aula_atividade_escola');
        Schema::dropIfExists('aula_atividade_classe');
        
    }
}
