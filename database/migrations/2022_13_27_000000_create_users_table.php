<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('foto')->nullable();
            $table->date('data_nascimento');
            $table->enum('role', ['ADMIN', 'GESTOR', 'PROFESSOR', 'ALUNO']);
            $table->enum('status', ['ACTIVE', 'DISABLED']);
            $table->unsignedBigInteger('endereco_id')->nullable();
            $table->foreign('endereco_id')->references('id')->on('enderecos');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('usuario_escola', function (Blueprint $table) {
            $table->unsignedBigInteger('usuario_id');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
            $table->unsignedBigInteger('escola_id');
            $table->foreign('escola_id')->references('id')->on('escolas');
            $table->enum('role', ['ADMIN', 'GESTOR', 'PROFESSOR', 'ALUNO']);
            $table->softDeletes();
        });

        Schema::create('usuario_classe', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('usuario_id');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
            $table->unsignedBigInteger('classe_id');
            $table->foreign('classe_id')->references('id')->on('classes');
            $table->enum('role', ['ADMIN', 'GESTOR', 'PROFESSOR', 'ALUNO']);
            $table->softDeletes();
        });

        Schema::create('usuario_alternativa', function (Blueprint $table) {
            $table->unsignedBigInteger('usuario_id');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
            $table->unsignedBigInteger('alternativa_id');
            $table->foreign('alternativa_id')->references('id')->on('alternativas');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('usuarios');
        Schema::dropIfExists('usuario_escola');
        Schema::dropIfExists('usuario_classe');
        Schema::dropIfExists('usuario_alternativa');
    }
}
