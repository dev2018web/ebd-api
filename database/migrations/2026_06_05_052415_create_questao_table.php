<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   

        Schema::create('questoes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('descricao');
            $table->unsignedBigInteger('atividade_id');
            $table->foreign('atividade_id')->references('id')->on('atividades');
            $table->boolean('multipla_escolha');
            $table->softDeletes();
            $table->timestamps();
        });
       
        Schema::create('usuario_nota_questao', function (Blueprint $table) {
            $table->unsignedBigInteger('questao_id');
            $table->foreign('questao_id')->references('id')->on('questoes');
            $table->unsignedBigInteger('usuario_id');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
            $table->decimal('nota', 8, 2);
            $table->softDeletes();
            $table->timestamps();
        });
        // OBS.: Colocar nota automática na tabela usuario_atividade
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('questao');
        Schema::dropIfExists('usuario_nota_questao');
    }
}
