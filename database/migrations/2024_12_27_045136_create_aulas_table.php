<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAulasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aulas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('licao_id');
            $table->foreign('licao_id')->references('id')->on('licoes');
            $table->string('titulo');
            $table->softDeletes();
            $table->timestamps();
        });


        Schema::create('aula_conteudo', function (Blueprint $table) {
            $table->unsignedBigInteger('aula_id');
            $table->foreign('aula_id')->references('id')->on('aulas');
            $table->unsignedBigInteger('conteudo_id');
            $table->foreign('conteudo_id')->references('id')->on('conteudos');
        });

        Schema::create('lessionada_classe', function (Blueprint $table) {
            $table->unsignedBigInteger('aula_id');
            $table->unsignedBigInteger('classe_id');
            $table->foreign('aula_id')->references('id')->on('aulas');
            $table->foreign('classe_id')->references('id')->on('classes');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('aulas');
        Schema::dropIfExists('aula_conteudo');
        Schema::dropIfExists('aula_classe');
    }
}
